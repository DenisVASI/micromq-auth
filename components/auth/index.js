const validate = require('validate.js');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const config = require('../../config/prod');
const {security: {jwt_secret}} = config;

const isNil = (value) => {
    return value === null || value === undefined || value === false;
};

const reg_validate = ({email, password} = {}) => {
    return new Promise(async resolve => {
        console.log({email, password});
        const constraints = {
            email: {
                presence: true,
                email: true
            },
            password: {
                presence: true,
                length: {
                    minimum: 10,
                    message: "must be at least 10 characters"
                },
                exclusion: {
                    message: "'%{value}' is not allowed"
                }
            }
        };
        const validate_result = validate({password, email}, constraints);
        resolve(isNil(validate_result) ? true : validate_result);
    });
};

const validate_pass = ({hash, password, salt} = {}) => {
    const local_hash = crypto.pbkdf2Sync(password, salt, 10, 512, 'sha512').toString('hex');
    return hash === local_hash;
};

const get_token = (req) => {
    const {headers: {authorization}} = req;
    if (authorization && authorization.split(' ')[0] === 'Token') {
        return authorization.split(' ')[1];
    }
    return null;
};

const auth_required = (req, res, next) => {
    const current_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const token = get_token(req);
    if (token) {
        jwt.verify(token, jwt_secret, function (err, decoded) {
            if (err) {
                res.status(401);
                res.json({
                    errors: [err.message]
                });
            } else {
                if (current_ip !== decoded.client_ip) {
                    console.log(decoded);
                    next(decoded);
                } else {
                    res.status(401);
                    res.json({
                        errors: ["different ip address"]
                    });
                }
            }
        });
    } else {
        res.status(401);
        res.json({
            errors: ["token required"]
        });
    }
};

module.exports = {reg_validate, validate_pass, auth_required};