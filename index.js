const config = require('./config/prod');
const Gateway = require('micromq/gateway');

const {rbmq_user, rbmq_password, rbmq_port, rbmq_address, gateway_port} = config;

const gateway = new Gateway({
    // названия микросервисов, к которым мы будем обращаться
    microservices: ['auth'],
    rabbit: {
        url: `amqp://${rbmq_user}:${rbmq_password}@${rbmq_address}:${rbmq_port}`,
    },
});

gateway.post(['/reg', '/auth', '/test'], async (req, res) => {
    await res.delegate('auth');
});

gateway.get(['/test'], async (req, res) => {
    await res.delegate('auth');
});

gateway.listen(gateway_port);