const config = require('../config/prod');
const MicroMQ = require('micromq');
const mysql = require('mysql');
const {rbmq_user, rbmq_password, rbmq_port, rbmq_address, memsql_address, memsql_port, memsql_login, memsql_pass, memsql_database} = config;


const connection = mysql.createConnection({
    host: memsql_address,
    port: memsql_port,
    user: memsql_login,
    password: memsql_pass,
    database: memsql_database
});

connection.connect();

const app = new MicroMQ({
    name: 'database',
    rabbit: {
        url: `amqp://${rbmq_user}:${rbmq_password}@${rbmq_address}:${rbmq_port}`,
    },
});

app.action('insert_user', async (meta, res) => {
    connection.query('INSERT INTO users(email, password) VALUES (?,?)', [meta.email, meta.password], (error) => {
        if (error) {
            res.status(500);
            res.json({status: 500, errors: error});
        } else {
            res.status(200);
            res.json({status: 200, errors: []});
        }
    });
});

app.action('get_user', async (meta, res) => {
    connection.query('SELECT * FROM users WHERE email = ? LIMIT 1', [meta.email], (error, rows) => {
        if (error) {
            res.status(500);
            res.json({status: 500, errors: error});
        } else {
            res.status(200);
            res.json({status: 200, user: rows, errors: []});
        }
    });
});

app.start();