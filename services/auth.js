const config = require('../config/prod');
const {rbmq_user, rbmq_password, rbmq_port, rbmq_address} = config;
const {reg_validate, validate_pass, auth_required} = require('../components/auth');
const MicroMQ = require('micromq');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const {security: {salt, jwt_secret}} = config;

const app = new MicroMQ({
    name: 'auth',
    microservices: ['database'],
    rabbit: {
        url: `amqp://${rbmq_user}:${rbmq_password}@${rbmq_address}:${rbmq_port}`,
    },
});

app.post('/reg', async function (req, res) {

    const email = req.body.email;
    const password = req.body.password;

    const is_valid = await reg_validate({
        password: password,
        email: email
    });

    if (is_valid === true) {

        const hash = crypto.pbkdf2Sync(password, salt, 10, 512, 'sha512').toString('hex');
        const {response} = await app.ask('database', {
            server: {
                action: 'insert_user',
                meta: {
                    password: hash,
                    email: email
                },
            },
        });

        res.status(response.status);
        res.json(response);
    } else {
        res.status(200);
        res.json({status: 200, errors: is_valid});
    }
});

app.post('/auth', async function (req, res) {
    const {email, password} = req.body;
    const {response} = await app.ask('database', {
        server: {
            action: 'get_user',
            meta: {
                email: email
            }
        }
    });

    if (response.user && response.user.length > 0) {
        const user = response.user[0];
        const is_password_valid = validate_pass({
            salt,
            password,
            hash: user.password
        });

        if (is_password_valid) {
            const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            const token = jwt.sign({
                data: {
                    client_ip: ip,
                    email: email
                }
            }, jwt_secret, {expiresIn: '1h'});
            res.status(200);
            res.json({
                token: token
            });
        } else {
            res.status(200);
            res.json({
                errors: ["email or password incorrect"]
            });
        }
    } else {
        res.status(200);
        res.json({
            errors: ["user was't found"]
        });
    }
});

app.get('/test', auth_required, function (req, res) {
    res.status(200);
    res.json({status: 200});
});

app.start();